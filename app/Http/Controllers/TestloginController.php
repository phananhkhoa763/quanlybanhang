<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class TestloginController extends Controller
{
    public function showLogin()
    {
        return view('Login_v1.index');
    }
    public function login(Request $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        $remember = false;

        if ($request->remember) {
            $remember = true;
        }
        if ($this->doLogin($login, $remember)) {
            return redirect()->route('frontend.blog.index');
        } else {
            return redirect()->back()->withErrors('Email or password is not correct.');
        }   
    }
    protected function doLogin($attempt, $remember)
    {

        if (Auth::attempt($attempt, $remember)) {
            return true;
        } else {
            return false;
        }
    }
}
